package br.com.digio.androidtest.data.model

import com.google.gson.annotations.SerializedName

data class SpotlightRequest(
    @SerializedName("bannerURL") private val bannerURL: String?,
    @SerializedName("name") private val name: String?,
    @SerializedName("description") private val description: String?
)
