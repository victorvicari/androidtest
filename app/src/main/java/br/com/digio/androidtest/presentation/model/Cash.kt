package br.com.digio.androidtest.presentation.model

data class Cash(
    val bannerURL: String,
    val title: String
)