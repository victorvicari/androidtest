package br.com.digio.androidtest.di.coroutine

import br.com.digio.androidtest.contextprovider.CoroutineContextProvider
import br.com.digio.androidtest.contextprovider.CoroutineContextProviderImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val coroutineModule = module {
    single<CoroutineContextProvider> { CoroutineContextProviderImpl(Dispatchers.IO) }
}