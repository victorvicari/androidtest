package br.com.digio.androidtest.di.repository

import br.com.digio.androidtest.data.Repository
import br.com.digio.androidtest.data.RepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    factory<Repository> { RepositoryImpl(get(), get()) }
}