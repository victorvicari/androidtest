package br.com.digio.androidtest.data

import br.com.digio.androidtest.data.model.DigioProductsRequest
import retrofit2.Call
import retrofit2.http.GET

interface DigioEndpoint {
    @GET("sandbox/products")
    fun getProducts(): Call<DigioProductsRequest>
}