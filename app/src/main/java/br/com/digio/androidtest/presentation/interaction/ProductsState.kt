package br.com.digio.androidtest.presentation.interaction

import br.com.digio.androidtest.data.model.DigioProductsRequest

sealed class ProductsState {
    data class Idle(
        val isLoading: Boolean = true,
        val products: DigioProductsRequest? = null,
        val error: Throwable? = null
    ) : ProductsState()

    data class OnSuccess(
        val isLoading: Boolean = false,
        val products: DigioProductsRequest?,
        val error: Throwable? = null
    ) : ProductsState()

    data class OnFailure(
        val isLoading: Boolean = false,
        val products: DigioProductsRequest? = null,
        val error: Throwable?
    ) : ProductsState()
}
