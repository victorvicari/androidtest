package br.com.digio.androidtest.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.data.Repository
import br.com.digio.androidtest.presentation.interaction.ProductsState
import kotlinx.coroutines.launch

class DigioViewModel(private val repository: Repository) : ViewModel() {

    private val _state = MutableLiveData<ProductsState>()
    val state: LiveData<ProductsState> = _state

    fun fetchProducts() {
        _state.postValue(ProductsState.Idle())
        viewModelScope.launch {
            val result = runCatching { repository.fetchProducts() }

            result.onSuccess { _state.postValue(ProductsState.OnSuccess(products = it)) }
                .onFailure { _state.postValue(ProductsState.OnFailure(error = it)) }
        }
    }
}