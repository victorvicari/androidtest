package br.com.digio.androidtest.presentation

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.presentation.adapter.ProductAdapter
import br.com.digio.androidtest.presentation.adapter.SpotlightAdapter
import br.com.digio.androidtest.presentation.interaction.ProductsState
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var txtMainDigioCash: TextView
    private lateinit var recyMainProducts: RecyclerView
    private lateinit var recyMainSpotlight: RecyclerView
    private lateinit var body: ConstraintLayout
    private lateinit var loadDigioContainer: ConstraintLayout

    private val viewModel: DigioViewModel by viewModel()

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    override fun onResume() {
        super.onResume()
        txtMainDigioCash = findViewById(R.id.txtMainDigioCash)
        recyMainProducts = findViewById(R.id.recyMainProducts)
        recyMainSpotlight = findViewById(R.id.recyMainSpotlight)
        body = findViewById(R.id.body)
        loadDigioContainer = findViewById(R.id.loadDigioContainer)

        recyMainProducts.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainProducts.adapter = productAdapter

        recyMainSpotlight.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainSpotlight.adapter = spotlightAdapter

        body.visibility = View.GONE
        loadDigioContainer.visibility = View.VISIBLE

        setupObservables()

        viewModel.fetchProducts()
        setupDigioCashText()
    }

    private fun setupObservables() {
        viewModel.state.observe(this, { state -> updateUi(state) })
    }

    private fun updateUi(state: ProductsState): Unit = when (state) {
        is ProductsState.Idle -> Unit
        is ProductsState.OnSuccess -> successState(state)
        is ProductsState.OnFailure -> failureState(state)
    }

    private fun successState(state: ProductsState.OnSuccess) {
        loadDigioContainer.visibility = View.GONE
        body.visibility = View.VISIBLE

        productAdapter.products = state.products?.products!!
        spotlightAdapter.spotlights = state.products.spotlight
    }

    private fun failureState(state: ProductsState.OnFailure) {
        val message = getString(R.string.error)

        loadDigioContainer.visibility = View.GONE
        body.visibility = View.GONE

        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun setupDigioCashText() {
        val digioCacheText = "digio Cache"
        txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)
                ),
                6,
                digioCacheText.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}