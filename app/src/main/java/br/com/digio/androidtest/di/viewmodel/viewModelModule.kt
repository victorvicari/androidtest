package br.com.digio.androidtest.di.viewmodel

import br.com.digio.androidtest.presentation.DigioViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { DigioViewModel(get()) }
}