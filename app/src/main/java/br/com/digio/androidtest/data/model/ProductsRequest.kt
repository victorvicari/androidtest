package br.com.digio.androidtest.data.model

import com.google.gson.annotations.SerializedName

data class ProductsRequest(
    @SerializedName("imageURL") private val imageURL: String?,
    @SerializedName("name") private val name: String?,
    @SerializedName("description") private val description: String?
)
