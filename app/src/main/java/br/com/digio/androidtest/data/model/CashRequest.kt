package br.com.digio.androidtest.data.model

import com.google.gson.annotations.SerializedName

data class CashRequest(
    @SerializedName("bannerURL") private val bannerURL: String?,
    @SerializedName("title") private val title: String?
)
