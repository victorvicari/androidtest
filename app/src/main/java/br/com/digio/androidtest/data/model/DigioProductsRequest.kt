package br.com.digio.androidtest.data.model

import br.com.digio.androidtest.presentation.model.Cash
import br.com.digio.androidtest.presentation.model.Product
import br.com.digio.androidtest.presentation.model.Spotlight
import com.google.gson.annotations.SerializedName

data class DigioProductsRequest(
    @SerializedName("cash") val cash: Cash?,
    @SerializedName("products") val products: List<Product> = emptyList(),
    @SerializedName("spotlight") val spotlight: List<Spotlight> = emptyList()
)
