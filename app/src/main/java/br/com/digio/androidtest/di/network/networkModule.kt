package br.com.digio.androidtest.di.network

import br.com.digio.androidtest.BuildConfig
import br.com.digio.androidtest.data.DigioEndpoint
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideDigioApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideOkHttpClient() = OkHttpClient.Builder()
    .build()

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

fun provideDigioApi(retrofit: Retrofit): DigioEndpoint = retrofit.create(DigioEndpoint::class.java)
