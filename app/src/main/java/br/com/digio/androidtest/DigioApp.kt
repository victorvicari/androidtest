package br.com.digio.androidtest

import android.app.Application
import br.com.digio.androidtest.di.coroutine.coroutineModule
import br.com.digio.androidtest.di.network.networkModule
import br.com.digio.androidtest.di.repository.repositoryModule
import br.com.digio.androidtest.di.viewmodel.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DigioApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            printLogger()
            androidContext(this@DigioApp)
            modules(
                networkModule +
                        repositoryModule +
                        viewModelModule +
                        coroutineModule
            )
        }
    }
}