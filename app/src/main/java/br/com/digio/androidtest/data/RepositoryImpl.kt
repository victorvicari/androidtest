package br.com.digio.androidtest.data

import br.com.digio.androidtest.contextprovider.CoroutineContextProvider
import br.com.digio.androidtest.data.model.DigioProductsRequest
import kotlinx.coroutines.withContext
import retrofit2.await

interface Repository {
    suspend fun fetchProducts(): DigioProductsRequest
}

class RepositoryImpl(
    private val service: DigioEndpoint,
    private val coroutineContextProvider: CoroutineContextProvider
) : Repository {

    override suspend fun fetchProducts(): DigioProductsRequest =
        withContext(coroutineContextProvider.context()) {
            service.getProducts().await()
        }
}