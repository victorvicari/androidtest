package br.com.digio.androidtest.presentation.model

data class Spotlight(
    val bannerURL: String,
    val name: String,
    val description: String
)