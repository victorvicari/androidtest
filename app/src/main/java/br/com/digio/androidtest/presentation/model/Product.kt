package br.com.digio.androidtest.presentation.model

data class Product(
    val imageURL: String,
    val name: String,
    val description: String
)